# ignisbot_setup
To launch this script you have to execute it in the JetBot Offitial Ubuntu ISO.
See wiki on how to create the ISO and install it in you JetSon NANO: https://www.waveshare.com/wiki/JetBot_AI_Kit
[Wiki Jetbot ISO install](https://www.waveshare.com/wiki/JetBot_AI_Kit)
Please note that abviously you need a jetson nano card for this to work.

Quickstart:
-----------
```shell
./setup_script_ignisbot_complete.sh
```
