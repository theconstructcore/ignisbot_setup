# Get info of GPU
# JetsonNano uses Tegra system ( https://devtalk.nvidia.com/default/topic/1055327/jetson-nano/gpu-usage-info-nvidia-smi-is-not-there-/ )
# So we cant use the "nvidia-smi" command to monitor GPU usage.
# Instead we use: https://docs.nvidia.com/jetson/l4t/index.html#page/Tegra%2520Linux%2520Driver%2520Package%2520Development%2520Guide%2FAppendixTegraStats.html%23wwconnect_header
echo "INSTALLING GPU RELATED APPS..."


sudo tegrastats

# Install Extra requirements for PeopleTRacker
sudo pip3 install --upgrade cython

# https://devtalk.nvidia.com/default/topic/1049071/jetson-nano/pytorch-for-jetson-nano-version-1-3-0-now-available/
wget https://nvidia.box.com/shared/static/phqe92v26cbhqjohwtvxorrwnmrnfx1o.whl -O torch-1.3.0-cp36-cp36m-linux_aarch64.whl
pip3 install numpy torch-1.3.0-cp36-cp36m-linux_aarch64.whl


sudo pip3 install --upgrade pip setuptools

git clone https://github.com/NVIDIA-AI-IOT/torch2trt
cd torch2trt
sudo python3 setup.py install


git clone https://github.com/NVIDIA-AI-IOT/jetcam
cd jetcam
sudo python3 setup.py install

# Setup Peopple tracker
sudo pip3 install tqdm cython pycocotools
sudo apt-get install python3-matplotlib
git clone https://github.com/NVIDIA-AI-IOT/trt_pose
cd trt_pose
sudo python3 setup.py install

echo "INSTALLING GPU RELATED APPS...DONE"

# INSTALL ROS RELATED APPS
echo "INSTALLING ROS RELATED APPS..."
sudo rosdep init 
rosdep update
echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc 
source ~/.bashrc
sudo apt-get install cmake python-catkin-pkg python-empy python-nose python-setuptools libgtest-dev python-rosinstall python-rosinstall-generator python-wstool build-essential git
mkdir -p ~/catkin_ws/src 
cd ~/catkin_ws/
catkin_make
echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc 
source ~/.bashrc


# Setup some istuff for camera scripts
sudo apt install libcanberra-gtk-module libcanberra-gtk3-module

# Install the teleop twist
sudo apt install ros-melodic-teleop-twist-keyboard
#PID
sudo apt install ros-melodic-pid

# Setup the python 3 venv needed for the people tracker
python3 --version
sudo apt-get install build-essential libssl-dev libffi-dev python-dev
sudo apt install -y python3-venv
sudo apt install virtualenv
sudo apt-get install python3-pip python3-yaml
sudo pip3 install rospkg catkin_pkg

echo "INSTALLING ROS RELATED APPS...DONE"
# Compile for python3
echo "INSTALLING IgnisBot Course RELATED APPS..."
cd /home/jetbot/catkin_ws
catkin_make -DPYTHON_EXECUTABLE:FILEPATH=/home/jetbot/catkin_ws/venv/bin/python
# Change the script shebang for python3

# Install dependencies for IgnisBot related packages
pip3 install rospkg


pip3 install torch torchvision

pip3 install --upgrade cython
pip3 install tqdm cython pycocotools

#sudo apt-get install python3-matplotlib
pip3 install matplotlib


# INstall cv2 for python3
pip3 install opencv-python

pip3 uninstall em
pip3 install empy

# Download OpenPose into catkin_ws
cd /home/jetbot/catkin_ws/src
git clone https://github.com/stevenjj/openpose_ros.git
mv openpose_ros/openpose_ros_msgs ./
rm -rf openpose_ros
cd /home/jetbot/catkin_ws
catkin_make -DPYTHON_EXECUTABLE:FILEPATH=/usr/bin/python3

cd /home/jetbot/catkin_ws/src
git clone https://github.com/ros-perception/vision_opencv.git
cd vision_opencv/
apt-cache show ros-melodic-cv-bridge | grep Version
# 1.13.0...
git checkout 1.13.0
cd /home/jetbot/catkin_ws
catkin_make -DPYTHON_EXECUTABLE:FILEPATH=/usr/bin/python3
source devel/setup.bash
# Test by, executing python, and : from cv_bridge.boost.cv_bridge_boost import getCvType


cd /home/jetbot/catkin_ws/src
git clone https://bitbucket.org/theconstructcore/ignisbot_jetbot.git
git clone https://bitbucket.org/theconstructcore/ignisbot_servohat.git

# For GUI Data collection
sudo apt-get install python3.6-tk
sudo apt-get install python-imaging-tk
sudo apt-get install python3-pil python3-pil.imagetk

echo "INSTALLING IgnisBot Course RELATED APPS..."

# Install RealRobot Connection for ROSDevelopementStudio
echo "INSTALLING RealRobotConnection..."
cd
git clone https://bitbucket.org/theconstructcore/rosds_real_robot_connection.git
sudo apt install curl
cd rosds_real_robot_connection; sudo ./realrobot_setup.sh
sudo reboot
echo "INSTALLING RealRobotConnection...DONE"

